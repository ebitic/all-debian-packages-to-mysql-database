# Developed by Evan BITIC
# 29/06/2023
# https://gitlab.com/ebitic

import requests
import mysql.connector

# DEBIAN API
debian_endpoint_packages_list = "https://sources.debian.org/copyright/api/list/"

# DB VARIABLES
db_host = ""
db_user = ""
db_password = ""
db_database = ""
db_sql_request = "INSERT INTO packages (name) VALUES (%s)"

# VARIABLES | Don't change them
packages_name = []

req = requests.get(debian_endpoint_packages_list)

if req.status_code == 200:
  for package in req.json()["packages"]:
      packages_name.append([package["name"]])
else:
  print("404 not found")
  exit()

try:
  db = mysql.connector.connect(
    host=db_host,
    user=db_user,
    password=db_password,
    database=db_database)
except:
  print("DB connection not working")
  exit()

cursor = db.cursor()

cursor.executemany(db_sql_request, packages_name)
db.commit()